/* window.vala
 *
 * Copyright 2021 Alexander Mikhaylenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate (ui = "/org/example/App/window.ui")]
public class ThreedLabyrinth.Window : Adw.ApplicationWindow {
    public bool dark_mode { get; set; }

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    static construct {
        typeof (LabyrinthView).ensure ();

        install_property_action ("window.dark-mode", "dark-mode");
    }

    construct {
        Adw.StyleManager.get_default ().notify["dark"].connect (() => {
            if (Adw.StyleManager.get_default ().dark)
                add_css_class ("dark");
            else
                remove_css_class ("dark");
        });

        if (Adw.StyleManager.get_default ().dark)
            add_css_class ("dark");
        else
            remove_css_class ("dark");

        notify["dark-mode"].connect (() => {
            Adw.StyleManager.get_default ().color_scheme = dark_mode ? Adw.ColorScheme.FORCE_DARK : Adw.ColorScheme.FORCE_LIGHT;
        });
    }

    [GtkCallback]
    private string get_dark_mode_icon (bool dark_mode) {
        return dark_mode ? "light-mode-symbolic" : "dark-mode-symbolic";
    }
}
